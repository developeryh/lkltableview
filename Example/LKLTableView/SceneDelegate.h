//
//  SceneDelegate.h
//  LKLTableView
//
//  Created by MasterFly on 2021/9/12.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

